import { useState } from 'react'
import { Box, Heading, Flex, Select } from '@chakra-ui/core'
import { partitionList } from '@/lib/utils'
import { BuacCSV } from '@/lib/race-folder'
import { buacCSVSlug } from '@/pages/files/csv/[csvSlug]'
import moment from 'moment'
import { startCase } from 'lodash'
import Layout from '@/components/Layout'
import CSVChooser from '@/components/CSVChooser'
import allRaceCSVLite from '@/json/allRaceCSVLite.json'
import allYear from '@/json/allYear.json'

const Results = ({ allRaceCSVLite, allYear }) => {
  const years = allYear.map((y) => y, toString()).reverse()
  const [year, setYear] = useState(years[0])
  const raceGroup = partitionList(
    allRaceCSVLite.filter((c: BuacCSV) => c.raceFolder.year.toString() == year),
    (csv: BuacCSV) =>
      `${csv.raceFolder.year} ${csv.raceFolder.frontMatter.name}`
  )
    .reverse()
    .map((csvs: BuacCSV[]) => ({
      first: csvs[0], // Used to id all items in all groups
      groups: partitionList(csvs, (csv: BuacCSV) => csv.buacLength).map(
        (subGroup: BuacCSV[]) => ({
          csvs: subGroup,
          label: subGroup[0].buacLength.match(/\d\./)
            ? subGroup[0].buacLength
            : startCase(subGroup[0].buacLength.toLocaleLowerCase()),
          itemLabel: (c: BuacCSV) => c.csvType,
        })
      ),
    }))

  return (
    <Layout>
      <Heading>Results {year}</Heading>
      <Select
        placeholder="Year"
        onChange={(e) => {
          e.preventDefault()
          setYear(e.target.value)
        }}
      >
        {years.map((y: string) => (
          <option key={y} value={y}>
            {y}
          </option>
        ))}
      </Select>
      <table>
        <tbody>
          {raceGroup.map(({ first, groups }) => (
            <tr key={buacCSVSlug(first)}>
              <th>
                {first.raceFolder.frontMatter.name}{' '}
                {moment(first.raceFolder.frontMatter.date).format(
                  'MMM Do YYYY'
                )}
              </th>
              <td>
                <Flex>
                  {groups.map((group) => (
                    <Flex key={group.label}>
                      <CSVChooser
                        csvs={group.csvs}
                        buttonTitle={group.label}
                        itemLabelFn={group.itemLabel}
                      />
                    </Flex>
                  ))}
                </Flex>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Layout>
  )
}
export default Results

export async function getStaticProps() {
  return {
    props: {
      allRaceCSVLite,
      allYear,
    },
  }
}
