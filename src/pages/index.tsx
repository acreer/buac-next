import Layout from '@/components/Layout'
import Link from 'next/link'
import {
  Box,
  Button,
  Link as UiLink,
  Flex,
  Heading,
  Text,
} from '@chakra-ui/core'
import NewsLink from '@/components/NewsLink'
import allPost from '@/json/allPost.json'

function HomePage({ recentPosts }) {
  return (
    <Layout>
      <Flex direction={['column-reverse', 'row']}>
        <Box width={2 / 5}>
          <Text colorScheme="Orange">Goose</Text>
        </Box>
        <Box>
          <NewsLink posts={recentPosts} />
        </Box>
      </Flex>
    </Layout>
  )
}

export default HomePage

export function getStaticProps() {
  return {
    props: { recentPosts: allPost.slice(0, 3) },
  }
}
