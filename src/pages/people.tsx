import Link from 'next/link'
import { useState } from 'react'
import { Heading, Box, Button, Select } from '@chakra-ui/core'
import { CheckIcon } from '@chakra-ui/icons'
import { kebabCase } from 'lodash'
import { BuacMember } from '@/lib/members'
import Layout from '@/components/Layout'
import allMember from '@/json/allMember.json'
import allClub from '@/json/allClub.json'
import { mainCols } from '@/components/Buttons'

export const memberSlug = (m: BuacMember): string =>
  `/member/${kebabCase(m.name ? m.name : `${m.first} ${m.last}`)}`

const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')

export default function People({ allMember, allClub }) {
  const sortedMembers = allMember.sort((a, b) => a.name.localeCompare(b.name))
  const [activeClub, setActiveClub] = useState(allClub[0].club)
  const [filterLetter, setFilterLetter] = useState('')
  const filteredMembers = sortedMembers.filter(
    m =>
      m.club === activeClub &&
      (filterLetter ? m.name.match(new RegExp(`\\b${filterLetter}`)) : true)
  )
  return (
    <Layout>
      <Heading>
        People in club {activeClub}:{filteredMembers.length}
      </Heading>
      <Box>
        {letters.map(l => (
          <Button
            key={l}
            sz="xxs"
            rightIcon={l === filterLetter ? <CheckIcon /> : null}
            onClick={() =>
              l === filterLetter ? setFilterLetter('') : setFilterLetter(l)
            }
          >
            {l}
          </Button>
        ))}
      </Box>
      <Select onChange={e => setActiveClub(e.target.value)} value={activeClub}>
        {allClub.map(({ club, count }) => (
          <option value={club} key={club}>
            {club}
          </option>
        ))}
      </Select>
      <Box>
        {filteredMembers.map((m: BuacMember) => (
          <Link key={m.id} href={memberSlug(m)}>
            <Button {...mainCols} size="sm">
              {m.name}
            </Button>
          </Link>
        ))}
      </Box>
    </Layout>
  )
}
export async function getStaticProps() {
  return {
    props: {
      allMember,
      allClub,
    },
  }
}
