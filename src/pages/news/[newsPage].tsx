import Layout from '@/components/Layout'
import NewsLink from '@/components/NewsLink'
import allPost from '@/json/allPost.json'
import { range } from 'lodash'
import Link from 'next/link'
import { Link as UiLink, Heading, Box, Text, Button } from '@chakra-ui/core'

export const NewsPagePagination = ({ pages, page }) => (
  <Box>
    {range(1, pages).map((p) => (
      <Link key={p} href={newsPageSlug(p)}>
        <Button>{p}</Button>
      </Link>
    ))}
  </Box>
)

const NewsPage = ({ page, posts, pages }) => {
  return (
    <Layout>
      <Heading>
        {page > 1 ? (
          <Link href={newsPageSlug(1)}>
            <Button>Previous</Button>
          </Link>
        ) : null}
        News Page {page} of {pages}
      </Heading>
      <Box>
        <NewsLink posts={posts} />
      </Box>
      <NewsPagePagination pages={pages} page={page} />
    </Layout>
  )
}

export default NewsPage

export const articlesPerPage = 5
export const pages = Math.ceil(allPost.length / articlesPerPage)

export const newsPageSlug = (p) => `/news/${p}`

export function getStaticPaths() {
  return {
    fallback: false,
    paths: range(1, pages).map((page) => ({
      params: { newsPage: page.toString() },
    })),
  }
}

export function getStaticProps({ params }) {
  const start = params.newsPage - 1
  return {
    props: {
      pages,
      posts: allPost.slice(start, start + articlesPerPage),
      page: params.newsPage,
    },
  }
}
