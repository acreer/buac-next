import { Heading, Box } from '@chakra-ui/core'
import Layout from '@/components/Layout'

// Good to have types from this consistant json
import allPost from '@/json/allPost.json'

const Article = ({ post: { title, content } }) => {
  return (
    <Layout>
      <Heading dangerouslySetInnerHTML={{ __html: title.rendered }} />
      <Box dangerouslySetInnerHTML={{ __html: content.rendered }} />
    </Layout>
  )
}

export default Article

export const postSlug = (p) => {
  return `/files/news/${slugBase(p)}`
}

const slugBase = (p) => p.slug

export function getStaticPaths() {
  //const paths = allPost.map((p) => ({ params: { newsSlug: p.slug } }))
  return {
    fallback: false,
    paths: allPost.map((p) => ({ params: { newsSlug: slugBase(p) } })),
  }
}

export function getStaticProps({ params }) {
  const post = allPost.find((p) => slugBase(p) === params.newsSlug)
  return { props: { post } }
}
