// import Link from 'next/link'
import Layout from '@/components/Layout'
import { Box, Heading, Text, Flex } from '@chakra-ui/core'
import { kebabCase, startCase } from 'lodash'
import { basename } from 'path'
import { BuacCSV } from '@/lib/race-folder'
import { BuacMember } from '@/lib/members'
import SortedTable from '@/components/SortableTable'
import { memberSlug } from '@/pages/people'
import { partitionList } from '@/lib/utils'
import CSVChooser from '@/components/CSVChooser'

// Import the json and get types they say. Its good they say. Well (MATE) we already
// have types and things break when ts looks at the js not the classes in
// race-folder.ts
// import allRaceCSV from '@/json/allRaceCSV.json'
// import { memberSlug } from '@/pages/people'
// import allMember from '@/json/allMember.json'
const allRaceCSV = require('@/json/allRaceCSV.json')
const allRaceCSVLite = require('@/json/allRaceCSVLite')
//const allCSV = require('@/json/allCSV.json')
const allMember = require('@/json/allMember.json')

interface LengthGroup {
  csvs: BuacCSV[]
  label: string
  itemLabel?: (c: BuacCSV) => string
}

const FileList = ({
  csv,
  lengthGroups,
}: {
  csv: BuacCSV
  lengthGroups: LengthGroup[]
}) => (
  <Layout>
    <Box>
      <Heading>
        {csv.raceFolder.year} {csv.raceFolder.frontMatter.name}
      </Heading>
      <Text>
        {csv.buacLength} {csv.csvType}
      </Text>

      <Flex>
        {lengthGroups.map((group) => (
          <Flex key={group.label}>
            <CSVChooser
              activeCSV={csv}
              csvs={group.csvs}
              buttonTitle={group.label}
              itemLabelFn={(c: BuacCSV) => c.csvType}
            />
          </Flex>
        ))}
      </Flex>
      <SortedTable data={csv.rows} />
    </Box>
  </Layout>
)
/*
 */
export default FileList

const csv2slug = (csv: BuacCSV): string =>
  csv
    ? kebabCase(
        `${csv.raceFolder.slug}-${csv.buacType}-${csv.buacLength}-${basename(
          csv.fn,
          '.csv'
        )}`
      )
    : 'dunno'

export const buacCSVSlug = (csv: BuacCSV): string =>
  `/${['files', 'csv', csv2slug(csv)].join('/')}`

export async function getStaticPaths() {
  return {
    fallback: false,
    paths: allRaceCSV.map((c: BuacCSV) => ({
      params: { csvSlug: csv2slug(c) },
    })),
  }
}

export async function getStaticProps({ params }) {
  const csv: BuacCSV = allRaceCSV.find((c) => params.csvSlug === csv2slug(c))
  csv.rows = csv.rows.map((r: any) => ({
    ...r,
    slug: memberSlug(allMember.find((m: BuacMember) => m.id === r.member)),
  }))
  const sameDay = allRaceCSVLite.filter(
    (c: BuacCSV) => csv.raceFolder.folder === c.raceFolder.folder
  )
  // TODO refactor in results.
  const lengthGroups: LengthGroup[] = partitionList(
    sameDay,
    (c: BuacCSV) => c.buacLength
  ).map((subGroup: BuacCSV[]) => ({
    csvs: subGroup,
    label: subGroup[0].buacLength.match(/\d\./)
      ? subGroup[0].buacLength
      : startCase(subGroup[0].buacLength.toLocaleLowerCase()),
    //itemLabel: (c: BuacCSV) => c.csvType,
  }))

  return {
    props: {
      csv,
      lengthGroups,
    },
  }
}
