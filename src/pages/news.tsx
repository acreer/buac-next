import Layout from '@/components/Layout'
import allPost from '@/json/allPost.json'
import Link from 'next/link'
import { Link as UiLink } from '@chakra-ui/core'

import { postSlug } from '@/pages/files/news/[newsSlug]'
import { NewsPagePagination, pages } from '@/pages/news/[newsPage]'

const News = ({ allPost }) => {
  return (
    <Layout>
      {allPost.map((p) => (
        <li key={p.slug}>
          <Link href={postSlug(p)}>
            <UiLink dangerouslySetInnerHTML={{ __html: p.title.rendered }} />
          </Link>
        </li>
      ))}
      <NewsPagePagination pages={pages} />
    </Layout>
  )
}

export default News

export function getStaticProps() {
  return { props: { pages, allPost: allPost.slice(0, 5) } }
}
