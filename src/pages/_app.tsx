import React from 'react'
import { ChakraProvider, CSSReset } from '@chakra-ui/core'
import defaultTheme from '@chakra-ui/theme'
import { AppProps } from 'next/app'

//const defaultColors = defaultTheme.colors.yellow.500='#I'

const theme = {
  ...defaultTheme,

  config: {
    initialColorMode: 'dark', // "light" | "dark"
  },
}

function App({ Component, pageProps }: AppProps): React.ReactNode {
  return (
    <ChakraProvider theme={theme}>
      <CSSReset />
      <Component {...pageProps} />
    </ChakraProvider>
  )
}
export default App

/* CSV */
// 052f5f,005377,06a77d,d5c67a,f1a208

/* Array */
// ["052f5f","005377","06a77d","d5c67a","f1a208"]
