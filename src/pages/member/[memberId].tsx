import Link from 'next/link'
import Layout from '@/components/Layout'
import { Box, Button } from '@chakra-ui/core'
import { BuacMember } from '@/lib/members'
import { BuacCSV } from '@/lib/race-folder'
import { basename } from 'path'
import { memberSlug } from '@/pages/people'
import { partitionList } from '@/lib/utils'
import { buacCSVSlug } from '@/pages/files/csv/[csvSlug]'
import members from '@/json/allMember.json'
import allMemberCSV from '@/json/allMemberCSV.json'
import { mainCols } from '@/components/Buttons'

const MemberPage = ({
  member,
  test,
  csvsByRace,
}: {
  member: BuacMember
  test: any
  csvsByRace: BuacCSV[][]
}) => (
  <Layout>
    <Box>
      {member.name} {member.club}
    </Box>
    <Box>
      {csvsByRace.map((raceCSVs: BuacCSV[]) => (
        <Box key={raceCSVs[0].fn}>
          <Box bg="red">
            {raceCSVs[0].raceFolder.year}{' '}
            {raceCSVs[0].raceFolder.frontMatter.name}{' '}
            <div>
              {raceCSVs.map((csv: BuacCSV) => (
                <Link key={buacCSVSlug(csv)} href={buacCSVSlug(csv)}>
                  <Button {...mainCols} size="xs">
                    {csv.buacLength} {csv.csvType}
                  </Button>
                </Link>
              ))}
            </div>
          </Box>
        </Box>
      ))}
    </Box>
  </Layout>
)

export default MemberPage

const memberToId = (m: BuacMember) => basename(memberSlug(m))

export async function getStaticPaths() {
  // const members = readJSONData('allMember')
  return {
    fallback: false,
    paths: members.map((m: BuacMember) => ({
      params: { memberId: memberToId(m) },
    })),
  }
}

export async function getStaticProps({ params }) {
  // const members = await getLoadedMembers()
  // const members = readJSONData('allMember')
  const member = members.find((m) => memberToId(m) === params.memberId)
  const memberCSV: any = allMemberCSV.find(
    ({ member: eachMember, csvs }: { member: BuacMember; csvs: BuacCSV[] }) =>
      member.id === eachMember.id
  )
  // console.log(memberCSV)

  return {
    props: {
      member,
      csvsByRace: partitionList(
        memberCSV.csvs,
        (csv: BuacCSV) => csv.raceFolder.folder
      ),
    },
  }
}
