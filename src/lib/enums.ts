export enum BuacTypes {
  race = 'RACE',
  handicap = 'HANDICAP',
  speedPoints = 'SPEED_POINTS',
  clubPoints = 'CLUB_POINTS',
  dunno = 'DUNNO',
}
