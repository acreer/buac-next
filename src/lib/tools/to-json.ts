import chalk from 'chalk'
import {
  readFileSync,
  mkdirSync,
  writeFileSync,
  readdirSync,
  statSync,
} from 'fs'
import { join, dirname } from 'path'
import { flatten, uniq } from 'lodash'
import { BuacMembers } from '../members'
import { RaceFolder, BuacCSV } from '../race-folder'
import { BuacTypes } from '../enums'

import {
  partitionList,
  buacCSVGenders,
  fastestRunner,
  removeRows,
} from '../utils'

/*
Next js wants to have each dynamic route independant of others.

Buac wants the
  - race data to have links to members,
  - members to be updated by people in races
  - maybe we should use a db or graphql
  - I am going to save stuff as json files.

  */

export function walk(dir: string): string[] {
  let results: string[] = []
  readdirSync(dir).forEach((dirEntry: string) => {
    const walkPath: string = join(dir, dirEntry)
    const stat = statSync(walkPath)
    if (stat && stat.isDirectory()) {
      results = results.concat(walk(walkPath))
    } else {
      results.push(walkPath)
    }
  })
  return results
}

const saveObject = (obj: any, filename: string) => {
  mkdirSync(dirname(filename), { recursive: true })
  writeFileSync(filename, JSON.stringify(obj, null, 2))
}

const dataDir = '../buac-gatsby/src/race-data'
// json dir mentioned in tsconfig aslo
const jsonDir = './tmp-json'
;(async function () {
  mkdirSync(jsonDir, { recursive: true })
  const memberDB = new BuacMembers(
    await BuacMembers.loadMembers(join(dataDir, 'member.csv'))
  )
  console.log(
    chalk.green(`Found ${memberDB.loadedMembers.length} members in the file`)
  )

  const allRaceFolder: RaceFolder[] = walk(dataDir)
    .filter((fn: string) => fn.match(/frontMatter\.yaml/))
    .map((fn: string) => new RaceFolder(fn))
    .sort(
      (a: RaceFolder, b: RaceFolder) =>
        b.frontMatter.date.valueOf() - a.frontMatter.date.valueOf()
    )
  // console.log(allRaceFolder[0])
  console.log(chalk.green(`loaded ${allRaceFolder.length} RaceFolders`))

  console.log(chalk.green('Loading Race csv files '))
  const allCSV = flatten(
    await Promise.all(
      allRaceFolder.map(
        async (raceFolder) => await raceFolder.loadCsvs(memberDB)
      )
    )
  ).sort((a, b) => {
    if (a.raceFolder.frontMatter.date == b.raceFolder.frontMatter.date) {
      return b.rows.length - a.rows.length
    }
    return (
      b.raceFolder.frontMatter.date.valueOf() -
      a.raceFolder.frontMatter.date.valueOf()
    )
  })
  console.log(
    chalk.green(`Now there are  ${memberDB.loadedMembers.length} members`)
  )

  console.log(chalk.green('Making CSVLite no data races'))
  // everything but the data rows.
  const allCSVLite = allCSV.map((csv) => removeRows(csv))
  const allYear = uniq(allCSVLite.map((csv) => csv.raceFolder.year())).sort()
  const allType = uniq(allCSVLite.map((csv) => csv.buacType))
  const allLength = uniq(allCSVLite.map((csv) => csv.buacLength))
  const allMember = memberDB.members()
  const allClub = partitionList(allMember, (m) => m.club).map((members) => ({
    club: members[0].club,
    count: members.length,
  }))
  const allRaceCSV = allCSV.filter((csv) => csv.buacType == BuacTypes.race)

  console.log(chalk.green('getting handicap and fastest times'))
  const raceWinners = flatten(
    allRaceFolder
      .map((raceFolder) => {
        // Race Folders are 'lite' and dont contain the csvs.
        // inefficient at build time is better.
        const raceCSV = allRaceCSV.filter(
          (csv) => csv.raceFolder === raceFolder
        )
        // console.log('debug', raceFolder.frontMatter.filename)
        return partitionList(raceCSV, (csv) => csv.buacLength)
          .filter((lengthCSVs) => lengthCSVs.length == 1) // TODO remove this line to proccess half mara invite etc.
          .map((lengthCSVs) => ({
            raceFolder,
            length: lengthCSVs[0].buacLength,
            clubWinner: lengthCSVs[0].rows[0],
            // Easy when there is only one file per length
            fastestMale: fastestRunner(lengthCSVs[0], 'M', memberDB),
            fastestFemale: fastestRunner(lengthCSVs[0], 'F', memberDB),
            csv: removeRows(lengthCSVs[0]),
          }))
      })
      .filter((lengthList) => lengthList.length > 0) // remove future races with no results yet
  )
  console.log(chalk.green('Trawling every run for every member'))
  const allMemberCSV = allMember.map((member) => ({
    member,
    csvs: allRaceCSV
      .filter((csv) => csv.rows.some((row) => row.member == member.id))
      .map((csv) => removeRows(csv)),
  }))
  const allRaceCSVLite = allRaceCSV.map((csv: BuacCSV) => removeRows(csv))

  const allPost = flatten(
    walk(join(jsonDir, '/wp-cache/')).map((fn) =>
      JSON.parse(readFileSync(fn).toString())
    )
  )
  console.log(chalk.green(`Got ${allPost.length} WP posts`))

  console.log(chalk.green(`Saving json to ${jsonDir}`))

  saveObject(memberDB.loadedMembers, join(jsonDir, 'allMember.json'))
  saveObject(allYear, join(jsonDir, 'allYear.json'))
  saveObject(allLength, join(jsonDir, 'allLength.json'))
  saveObject(allType, join(jsonDir, 'allType.json'))
  saveObject(allClub, join(jsonDir, 'allClub.json'))
  saveObject(raceWinners, join(jsonDir, 'raceWinners.json'))
  saveObject(allMemberCSV, join(jsonDir, 'allMemberCSV.json'))
  saveObject(allCSV, join(jsonDir, 'allCSV.json'))
  saveObject(allCSVLite, join(jsonDir, 'allCSVLite.json'))
  saveObject(allRaceCSV, join(jsonDir, 'allRaceCSV.json'))
  saveObject(allRaceCSVLite, join(jsonDir, 'allRaceCSVLite.json'))
  saveObject(allPost, join(jsonDir, 'allPost.json'))
})()
