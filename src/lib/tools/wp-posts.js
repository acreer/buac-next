const axios = require('axios')
const fs = require('fs')
const path = require('path')

const api = axios.create({
  baseURL: 'http://bendigouniathsclub.org.au/blog/wp-json/wp/v2',
})

wpCache = path.join('.', 'tmp-json', 'wp-cache')
const per_page = 20

const ensure = (folder) => {
  if (!fs.existsSync(folder)) {
    fs.mkdirSync(folder, { recursive: true })
  }
  return folder
}

const save = (fn, content) => {
  // TODO caching
  fs.writeFileSync(fn, content)
  return fn
}

;(async () => {
  const postHead = await api.head('/posts', { params: { per_page } })
  const postDir = ensure(path.join(wpCache, 'post'))
  const postPages = parseInt(postHead.headers['x-wp-totalpages'], 10)

  console.log(`Getting ${postPages} posts pages with ${per_page}`)
  for (i = 1; i <= postPages; i++) {
    console.log(`Page ${i} of ${postPages}`)
    const postsPage = await api.get('/posts', {
      //_embed leaves authors in data,
      params: { per_page, page: i, _embed: true },
    })
    save(
      path.join(postDir, `page${i}.json`),
      JSON.stringify(postsPage.data, null, ' ')
    )
  }
})()
