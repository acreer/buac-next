import { readFileSync, statSync } from 'fs'
import { basename, join, dirname } from 'path'
import chalk from 'chalk'
import YAML from 'yaml'
import Papa from 'papaparse'
import { transform, camelCase, kebabCase } from 'lodash'
import { walk } from './tools/to-json'

import { BuacTypes } from './enums'
import { BuacMembers, BuacMember } from './members'

function csvType(fn: string): string {
  const base = basename(fn)
  let gender: string = ''
  let ageGroup: string = ''
  let fast: string = ''
  const all: string = ''

  if (base.match(/ALL/)) {
    fast = 'All'
  }
  if (base.match(/FASTEST/)) {
    fast = 'Fastest'
  }
  if (base.match(/female|women/i)) {
    gender = 'Female'
  } else if (base.match(/men|male/i)) {
    gender = 'Male'
  }

  if (base.match(/^(\d\d)-/)) {
    // 40-female, NOT 2019-Uni
    ageGroup = `${RegExp.$1} Plus`
  } else if (base.match(/open/i)) {
    ageGroup = 'Open'
  } else if (fn.match(/(\d+)[-_]*plus/)) {
    ageGroup = `${RegExp.$1} Plus`
  } else if (base.match(/u(nder)*[-_]*(\d+)/i)) {
    ageGroup = `Under ${RegExp.$2}`
  }
  const ret = [ageGroup, fast, all, gender].filter((s) => s.length).join(' ')
  // console.log(`${ret}FROM${fn}`)
  return ret
}

function buacType(fn: string): BuacTypes {
  if (
    fn.match(/net_points/) ||
    fn.match(/agg_points/) ||
    fn.match(/club_champs/)
  ) {
    return BuacTypes.clubPoints
  }
  if (
    fn.match(/adjusted_points.csv/) ||
    fn.match(/adj_points/) ||
    fn.match(/speed_champs/) ||
    fn.match(/speed_points/)
  ) {
    return BuacTypes.speedPoints
  }
  if (fn.match(/handicap/i)) {
    return BuacTypes.handicap
  }
  if (fn.match(/result/)) {
    return BuacTypes.race
  }
  throw new Error(`No buacType for '${fn}'`)
  return BuacTypes.dunno
}

// Lengths are crucial. Throw if you cant find one.
const numWithDecimal = /\D(\d+[\d\.]*?)km/
const numOnly = /(\d*)km/
const stringLength = /(SHORT|MEDIUM|LONG)/i
export const buacLength = (fn: string) => {
  // first look for lengths
  if (fn.match(numWithDecimal) || fn.match(numOnly)) {
    return `${RegExp.$1} km`
  }

  // then for long/short/med
  if (fn.match(stringLength)) {
    return RegExp.$1.toUpperCase()
  }

  throw new Error(`No buacLength for '${fn}`)
}

export type BuacCSV = {
  fn: string
  raceFolder: RaceFolder
  buacLength: string
  buacType: string
  rows: any[]
  csvType: string
  // slug?: string
}

class FrontMatter {
  raceNumber: number

  date: Date

  name: string

  raceId: string

  filename: string

  // Ends up as useRouteData
  toJSON() {
    const { raceNumber, name, raceId, filename } = this
    const date = this.date.toUTCString()
    return {
      raceNumber,
      date,
      name,
      raceId,
      filename,
    }
  }

  constructor(fn: string) {
    this.filename = fn
    const raw: any = transform(
      YAML.parse(readFileSync(fn).toString()),
      (result: any, value: any, key: any) =>
        (result[camelCase(`${key}`)] = value)
    )
    this.raceNumber = raw.number
    this.date = new Date(raw.date)
    this.name = raw.name
    if (raw.runMd) {
      this.raceId = raw.runMd.replace(/\.md/, '')
    } else {
      this.raceId = kebabCase(raw.name)
      // console.log(chalk.red(`No runMD in ${fn} - using ${this.raceId}`), raw)
    }
  }
}

export class RaceFolder {
  folder: string

  frontMatter: FrontMatter

  // Ends up as useRouteData
  toJSON() {
    const { folder } = this
    const frontMatter = this.frontMatter.toJSON()
    const slug = this.slug()
    const year = this.year()
    return {
      folder,
      slug,
      year,
      frontMatter,
    }
  }

  constructor(folder: string) {
    if (statSync(folder).isDirectory()) {
      throw new Error(`TODO RaceFolder folder ${folder}`)
    } else {
      this.folder = dirname(folder)
      this.frontMatter = new FrontMatter(folder)
    }
  }

  slug() {
    return kebabCase(`${this.year()}-${this.frontMatter.raceId}`)
  }

  year() {
    return this.frontMatter.date.getFullYear().toString()
  }

  async loadCsvs(members: BuacMembers): Promise<BuacCSV[]> {
    return await Promise.all(
      walk(this.folder)
        .filter((fn) => fn.match(/\.csv$/))
        .map(async (fn) => {
          let rows = [{}]
          const { data: raw, errors } = Papa.parse(
            readFileSync(fn).toString().trim(),
            {
              header: true,
              dynamicTyping: true,
            }
          )
          // const raw = await csv({ checkType: true }).fromFile(fn)
          rows = raw.map((row: any) =>
            transform(row, (result: any, value: any, key: any) => {
              result[camelCase(key)] = value
            })
          )
          const allHeadsCount = rows.reduce((acc: any, row: any) => {
            Object.keys(row).forEach((head: string) => (acc[head] = true))
            return acc
          }, {})
          // some rows dont have all data
          const allHeads = Object.keys(allHeadsCount)
          rows = rows.map((row: any) =>
            allHeads.reduce((acc: any, head: string) => {
              acc[head] = row[head] || ''
              return acc
            }, {})
          )
          // Set the names
          rows.forEach((row: any, rdx: number) => {
            if (!row.member) {
              const name = row.name
                ? row.name
                : row.first && row.last
                ? `${row.first} ${row.last}`
                : null

              if (name) {
                let member = members.memberByName(name)
                if (member) {
                  row.member = member.id
                } else {
                  const id = kebabCase([this.slug(), name].join('-'))
                  // console.log(chalk.magenta(`Adding member ${id}`))
                  member = members.addMember({ ...row, id })
                }
                row.member = member.id
              } else {
                console.log(
                  chalk.red(
                    `Cant get a name for ${JSON.stringify(row)} in ${fn}:${
                      rdx + 1
                    } `
                  )
                )
              }
            }
          })
          const bl = buacLength(fn)
          const bt = buacType(fn)
          const ct = csvType(fn)
          const ret: BuacCSV = {
            fn,
            rows,
            raceFolder: this,
            buacType: bt,
            buacLength: bl,
            csvType: ct,
          }
          return ret
        })
    )
  }
}
