import { readFileSync } from 'fs'
import { join, resolve } from 'path'
import Papa from 'papaparse'
import { kebabCase } from 'lodash'
import { ArrowBackIcon } from '@chakra-ui/icons'
import { useAccordion } from '@chakra-ui/core'
import chalk from 'chalk'

export type Club =
  | 'BUAC'
  | 'HARRIERS'
  | 'SOUTH BENDIGO'
  | 'EAGLEHAWK'
  | 'BYM'
  | 'OTHER'

const clubAlias = [
  { use: 'BUAC', aka: ['UNI', 'BAC'] },
  { use: 'HARRIERS', aka: ['HARRIERS', 'Bendigo Harriers', 'HAR'] },
  {
    use: 'SOUTH BENDIGO',
    aka: ['SBE', 'SOUTH BGO', 'STH BGO', 'SOUTH', 'STH', 'SBG', 'STH BENDIGO'],
  },
  { use: 'OTHER', aka: ['INV', 'INVITATION', 'NONE'] },
  { use: 'BYM', aka: [] },
  { use: 'EAGLEHAWK', aka: ['EGWK', 'eYMCA', 'EGHWK', 'EWK'] },
  { use: 'BENDIGO YMCA', aka: ['BGO YMCA', 'BYMCA', 'BYM'] },
]
const consolidatedClub = (club: string): Club => {
  const found = clubAlias.find(
    ({ use, aka }) =>
      use.toLowerCase() === club.toLocaleLowerCase() ||
      aka.find((a) => a.toLowerCase() === club.toLowerCase())
  )
  if (found) return found.use as Club
  console.log(chalk.red(`No club for ${club}`))
  return club as Club
}

const nameLookup = [
  {
    use: 'Andrew Buchanan',
    aka: ['Andy Buchanan'],
  },
  {
    use: 'Joshua Feuerherdt',
    aka: ['Josh Feuerherdt'],
  },
]

const consolidateName = (name: string): string => {
  const found = nameLookup.find(({ aka }) => aka.indexOf(name) > -1)
  if (found) return found.use
  return name
}

const unpaidRegex = /^\$/
export function cleanName(name: string): string {
  return (
    name
      .replace(unpaidRegex, '')
      // "Jackon   Eadon"
      .replace(/\s\s+/, ' ')
      .trim()
  )
}

function fixMember(fixed: BuacMember): BuacMember {
  if (!fixed.name && fixed.first && fixed.last) {
    fixed.name = cleanName(`${fixed.first} ${fixed.last}`)
  }
  if (!fixed.club) {
    fixed.club = 'OTHER'
  }
  fixed.name = cleanName(fixed.name)
  if (fixed.first) {
    fixed.first = cleanName(fixed.first)
  }
  return fixed
}

export type BuacMember = {
  id: string
  name?: string
  first?: string
  last?: string
  gender?: 'M' | 'F'
  tag?: string
  club: Club
}
export class BuacMembers {
  loadedMembers: BuacMember[]

  static async loadMembers(filename: string): Promise<BuacMember[]> {
    const { data: members, errors } = Papa.parse(
      readFileSync(filename).toString(),
      {
        header: true,
      }
    )
    return members
      .filter(({ member }) => member)
      .map(({ first, last, sex, /* dob, */ member, tag }) => ({
        name: `${first} ${last}`,
        first,
        last,
        gender: sex,
        // dob: MembersDB.fixDOB(dob),
        id: member,
        tag,
        club: 'BUAC',
      }))
  }

  constructor(loadedMemebers: BuacMember[]) {
    this.loadedMembers = loadedMemebers
  }

  memberByName(name: string): BuacMember | null {
    const cleaned = consolidateName(cleanName(name))
    const matches = this.loadedMembers.filter(
      (member: BuacMember) =>
        member.name.toLocaleLowerCase() == cleaned.toLocaleLowerCase()
    )
    switch (matches.length) {
      case 1:
        return matches[0]
      case 0:
        return null
      default:
        throw new Error(`memberByName ${matches.length} matches for '${name}'`)
    }
  }

  memberById(id: string): BuacMember | null {
    const matches = this.loadedMembers.filter(
      (m: BuacMember) =>
        m.id.toString().toLowerCase() == id.toString().toLowerCase()
    )
    switch (matches.length) {
      case 1:
        return matches[0]
      case 0:
        return null
      default:
        throw new Error(`memberById '${matches.length}' members match '${id}'`)
    }
  }

  members(): BuacMember[] {
    return this.loadedMembers
  }

  addMember(member: BuacMember): BuacMember {
    const newMember = fixMember(member)
    if (newMember.club) {
      newMember.club = consolidatedClub(newMember.club)
    } else {
      newMember.club = 'OTHER'
    }
    this.loadedMembers.push(newMember)
    return newMember
  }
}
