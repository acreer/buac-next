// import { flatten } from 'lodash'
import { join } from 'path'
import { BuacCSV } from './race-folder'

// split a list into a list of lists according to the function. Preserves the
// order of the original list, ie the first item in the original list will be
// first item in the first list
export const partitionList = (list: any[], itemFn: any) => {
  // Augment original list with the values of the itemFn function for each item
  const listWithValues = list.map((item) => ({
    groupValue: itemFn(item),
    original: item,
  }))

  return (
    listWithValues
      // Get the uniqe values of the itemFn call
      .filter(
        (item, idx, list) =>
          list.findIndex((i: any) => i.groupValue === item.groupValue) === idx
      )
      // filter the original list for each of those values.
      .map((group: any) =>
        listWithValues.filter(
          (item: any) => group.groupValue == item.groupValue
        )
      )
      // Get the origanal items out of each item in the partitioned array
      .map((partition: any[]) => partition.map((item: any) => item.original))
  )
}

export const activeColors = (active: boolean) => (!active ? 'uni' : 'warning')

export function buacCSVGenders(buacCSV, gender, memberDb) {
  return buacCSV.rows.filter(({ member }) => {
    const memberRow = memberDb.memberById(member)
    if (memberRow) {
      return memberRow.gender === gender
    }
    // TODO look at these
    console.log(
      `TODO how does member:'${member}' in ${buacCSV.slug} not have a memberRow`
    )
    return false
  })
}
export function fastestRunner(buacCSV, gender, memberDb) {
  const timeSortField = ['netTime', 'time'].find((e) =>
    buacCSV.rows[0].hasOwnProperty(e)
  )

  if (timeSortField) {
    return buacCSVGenders(buacCSV, gender, memberDb).sort((a, b) =>
      a[timeSortField].toString().localeCompare(b[timeSortField].toString())
    )[0]
  }
  throw new Error(
    `cant sort for fastest time in with ${JSON.stringify(
      buacCSV.rows[0],
      null,
      ' '
    )}`
  )
}

export const removeRows = (csv: BuacCSV): BuacCSV => {
  const lite = { ...csv }
  lite.rows = []
  return lite
}
