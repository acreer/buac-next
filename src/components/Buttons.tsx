/* Object from coolers.org
const ob = {
  'Prussian Blue': '052f5f',
  'Blue Sapphire': '005377',
  'Green Munsell': '06a77d',
  Flax: 'd5c67a',
  Marigold: 'f1a208',
}
 */

import { Button, useColorMode } from '@chakra-ui/core'

const prussianBlue = '#052f5f'
const marigold = '#f1a208'

export const mainBg = prussianBlue
export const mainColor = marigold

export const mainCols = { bg: mainBg, color: mainColor }

export const MainButton = ({ children, ...rest }) => (
  <Button bg={mainBg} color={mainColor} {...rest}>
    {children}
  </Button>
)
