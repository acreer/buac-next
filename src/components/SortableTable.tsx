import React, { useState } from 'react'
import Link from 'next/link'
import { Text, Box, Link as ChLink } from '@chakra-ui/core'
import { startCase } from 'lodash'
import { MainButton, mainColor } from '@/components/Buttons'

const TableText = ({ children, ...rest }) => (
  <Text fontSize="sm" {...rest}>
    {children}
  </Text>
)

const csvSort = (data: any[], key: string, desc: boolean) => {
  if (!key) {
    return data
  }
  const sorted = data.sort((a, b) =>
    a[key].toString().localeCompare(b[key].toString(), 'en', { numeric: true })
  )
  return desc ? sorted.reverse() : sorted
}

type Props = {
  data: any[]
}

export default function SortedTable({ data }: Props) {
  const cols = Object.keys(data[0]).filter(
    (c: string) => c !== 'member' && c !== 'slug'
  )
  const [sortCol, setSortCol] = useState(cols[0])
  const [sortDesc, setSortDesc] = useState(false)

  const sortedData = csvSort(data, sortCol, sortDesc)

  const handleClick = (col: string) => {
    if (col == sortCol) {
      setSortDesc(!sortDesc)
    } else {
      setSortDesc(false)
      setSortCol(col)
    }
  }
  const colStyle = { padding: '5px 10px', textAlign: 'center' as const }
  return (
    <table>
      <thead>
        <tr>
          {cols.map((c: string, cx: number) => (
            <td key={cx} style={colStyle}>
              <MainButton size="xs" onClick={() => handleClick(c)}>
                {startCase(c)}
              </MainButton>
            </td>
          ))}
        </tr>
      </thead>
      <tbody>
        {sortedData.map((row: any) => (
          <tr key={row.slug}>
            {cols.map((c: string) => (
              <td key={c} style={colStyle}>
                {['first', 'last', 'name'].includes(c) ? (
                  <TableText color={mainColor}>
                    <Link href={row.slug} passHref>
                      <ChLink color="teal.500">{row[c]}</ChLink>
                    </Link>
                  </TableText>
                ) : (
                  <TableText>{row[c]}</TableText>
                )}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  )
}
