import React from 'react'
import { Select, Button, Flex } from '@chakra-ui/core'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { BuacCSV } from '@/lib/race-folder'
// import { activeColors } from '../lib/ts-utils'
import { buacCSVSlug } from '@/pages/files/csv/[csvSlug]'
import { MainButton, mainBg, mainColor } from '@/components/Buttons'

type Props = {
  activeCSV?: BuacCSV
  csvs: BuacCSV[]
  buttonTitle: string
  itemLabelFn: any
}
const CSVChooser = ({ csvs, buttonTitle, itemLabelFn, activeCSV }: Props) => {
  const router = useRouter()
  return csvs.length === 1 ? (
    <Link href={buacCSVSlug(csvs[0])}>
      <MainButton
        variant={
          buacCSVSlug(activeCSV) === buacCSVSlug(csvs[0]) ? 'outline' : 'solid'
        }
      >
        {buttonTitle}
      </MainButton>
    </Link>
  ) : (
    <Select
      bg={mainBg}
      color={mainColor}
      onChange={(e) => router.push(e.target.value)}
      placeholder={buttonTitle}
    >
      {csvs.map((csv: BuacCSV) => (
        <option key={buacCSVSlug(csv)} value={buacCSVSlug(csv)}>
          {itemLabelFn(csv)}
        </option>
      ))}
    </Select>
  )
}
export default CSVChooser
