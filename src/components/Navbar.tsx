import React from 'react'
import Link from 'next/link'
import {
  Flex,
  Button,
  IconButton,
  Box,
  useColorMode,
  DarkMode,
} from '@chakra-ui/core'
import { SunIcon, MoonIcon } from '@chakra-ui/icons'

import { mainCols } from '@/components/Buttons'

const Navbar = () => {
  const { colorMode, toggleColorMode } = useColorMode()
  return (
    <Flex px={[0, 2]} alignItems="center">
      <Link href="/">
        <Button {...mainCols}>Home</Button>
      </Link>
      <Link href="/results">
        <Button {...mainCols}>Results</Button>
      </Link>
      <Link href="/people">
        <Button {...mainCols}>People</Button>
      </Link>
      <Link href="/news">
        <Button {...mainCols}>News</Button>
      </Link>
      <DarkMode>
        <Box mx="auto" />
      </DarkMode>
      <Link href="/about">
        <Button {...mainCols}>About</Button>
      </Link>
      <IconButton
        {...mainCols}
        onClick={toggleColorMode}
        aria-label="mode button"
        icon={
          colorMode === 'light' ? (
            <MoonIcon arial-label="moon" />
          ) : (
            <SunIcon aria-label="sun" />
          )
        }
      />
    </Flex>
  )
}
/*
      <IconButton
        {...mainCols}
        onClick={toggleColorMode}
        ariaLabel="dark or light"
        icon={colorMode === 'light' ? <MoonIcon /> : <SunIcon />}
      />


      <Link href="/theme">
        <Button {...mainCols}>Theme</Button>
      </Link>
 */
export default Navbar
