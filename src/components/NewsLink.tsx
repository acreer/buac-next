import React from 'react'
import { Box, Heading, Link as UiLink } from '@chakra-ui/core'
import Link from 'next/link'

import { postSlug } from '@/pages/files/news/[newsSlug]'
/*
type Post = {
  slug: string
  title: {
    rendered: string
  }
  excerpt: {
    rendered: string
  }
}
const NewsLink = ({ posts }: { posts: Post[] }) => (
*/

const NewsLink = ({ posts }) =>
  posts.map((p) => (
    <Box key={p.slug}>
      <Link href={postSlug(p)}>
        <Box>
          <Heading color="orange" as="h4">
            <UiLink
              color="teal"
              dangerouslySetInnerHTML={{ __html: p.title.rendered }}
            />
          </Heading>
          <UiLink dangerouslySetInnerHTML={{ __html: p.excerpt.rendered }} />
        </Box>
      </Link>
    </Box>
  ))

export default NewsLink
