import React from 'react'
import { Box } from '@chakra-ui/core'
import Navbar from './Navbar'

const Layout = ({ children }) => (
  <Box p={2}>
    <Navbar />
    <section>{children}</section>
  </Box>
)

export default Layout
